# Link collection of ETW-related topics and resources

## EventSource
- [EventSource User’s Guide](https://github.com/Microsoft/dotnet-samples/blob/master/Microsoft.Diagnostics.Tracing/EventSource/docs/EventSource.md)
    - Or (probably) the same in docx (but probably more outdated, too): [EventSourceUsersGuide.docx](https://github.com/arphox/ETW-via-csharp/raw/master/resources/official/_EventSourceUsersGuide.docx) ([backup link](https://msdnshared.blob.core.windows.net/media/MSDNBlogsFS/prod.evol.blogs.msdn.com/CommunityServer.Components.PostAttachments/00/10/44/08/22/_EventSourceUsersGuide.docx))
- [Write to the Windows Event Log via ETW](https://devblogs.microsoft.com/dotnet/announcing-the-eventsource-nuget-package-write-to-the-windows-event-log/)

## TraceEvent
- [The TraceEvent Library Programmers Guide](https://github.com/Microsoft/perfview/blob/master/documentation/TraceEvent/TraceEventProgrammersGuide.md)
    - Or (probably) the same in docx version: [TraceEventProgrammersGuide.docx](https://github.com/arphox/ETW-via-csharp/raw/master/resources/official/_TraceEventProgrammersGuide.docx) ([backup link](https://msdnshared.blob.core.windows.net/media/MSDNBlogsFS/prod.evol.blogs.msdn.com/CommunityServer.Components.PostAttachments/00/10/44/21/52/_TraceEventProgrammersGuide.docx))
    
- [The Microsoft.Diagnostics.Tracing.TraceEvent Library](https://github.com/Microsoft/perfview/blob/master/documentation/TraceEvent/TraceEventLibrary.md)

- [TraceEvent samples](https://github.com/Microsoft/perfview/tree/master/src/TraceEvent/Samples), also available as a [NuGet package](https://www.nuget.org/packages/Microsoft.Diagnostics.Tracing.TraceEvent.Samples/).

## EventRegister
- [EventRegister User’s Guide](https://github.com/Microsoft/dotnet-samples/blob/master/Microsoft.Diagnostics.Tracing/EventSource/docs/EventRegister.md)
    - [docx version](https://github.com/arphox/ETW-via-csharp/raw/master/resources/official/_EventRegisterUsersGuide.docx)


## PerfView
- [PerfView GitHub](https://github.com/Microsoft/perfview)
- [PerfView GitHub / TraceEvent](https://github.com/Microsoft/perfview/tree/master/documentation/TraceEvent)
- [PerfView User's Guide](http://htmlpreview.github.io/?https://github.com/Microsoft/perfview/blob/master/src/PerfView/SupportFiles/UsersGuide.htm)


## [Vance Morrison](https://blogs.msdn.microsoft.com/vancem/)'s topics

1. [Introduction Tutorial: Logging ETW events in C#: System.Diagnostics.Tracing.EventSource](https://blogs.msdn.microsoft.com/vancem/2012/07/09/introduction-tutorial-logging-etw-events-in-c-system-diagnostics-tracing-eventsource/)
: Intro, how to write a minimal `EventSource` implementation, how to watch the results in **PerfView**. Demo app exported from the link, if it would be dead: [link](resources/official/EventSourceDemoV3.5.zip)

1. [The specification for the System.Diagnostics.Tracing.EventSource class.](https://blogs.msdn.microsoft.com/vancem/2012/07/09/the-specification-for-the-system-diagnostics-tracing-eventsource-class/)
: Link to `StronglyTypedEvents.docx`, can be found in this repo, too: [link](resources/official/StronglyTypedEvents.docx)

1. [Windows high speed logging: ETW in C#/.NET using System.Diagnostics.Tracing.EventSource](https://blogs.msdn.microsoft.com/vancem/2012/08/13/windows-high-speed-logging-etw-in-c-net-using-system-diagnostics-tracing-eventsource/)
: What/why ETW, what problems `EventSource` solves, how to write a tiny `EventSource` implementation to produce events.

1. [ETW in C#: Controlling which events get logged in an System.Diagnostics.Tracing.EventSource](https://blogs.msdn.microsoft.com/vancem/2012/08/14/etw-in-c-controlling-which-events-get-logged-in-an-system-diagnostics-tracing-eventsource/)

1. [An End-To-End ETW Tracing Example: EventSource and TraceEvent](https://blogs.msdn.microsoft.com/vancem/2012/12/20/an-end-to-end-etw-tracing-example-eventsource-and-traceevent/)
: item linked in the post: [link](resources/official/SimpleMonitor.zip)

1. [Using TraceSource to log ETW data to a file](https://blogs.msdn.microsoft.com/vancem/2012/12/20/using-tracesource-to-log-etw-data-to-a-file/)
: item linked in the post: [link](resources/official/MonitorToFile.zip)

1. [Why doesn’t my EventSource produce any events?](https://blogs.msdn.microsoft.com/vancem/2012/12/21/why-doesnt-my-eventsource-produce-any-events/)

1. [More Support for EventSource and strongly typed logging: The Semantic Logging Application Block](https://blogs.msdn.microsoft.com/vancem/2013/03/09/more-support-for-eventsource-and-strongly-typed-logging-the-semantic-logging-application-block/)

1. [The EventSource NuGet package and support for the Windows Event Log (Channel Support)](https://blogs.msdn.microsoft.com/vancem/2013/08/10/the-eventsource-nuget-package-and-support-for-the-windows-event-log-channel-support/)
: item linked in the post: [link](resources/official/_EventSourceUsersGuide.docx)

1. [TraceEvent ETW Library published as a NuGet Package](https://blogs.msdn.microsoft.com/vancem/2013/08/15/traceevent-etw-library-published-as-a-nuget-package/)
: item linked in the post: [link](resources/official/_TraceEventProgrammersGuide.docx)
[Link to the nuGet package](https://www.nuget.org/packages/Microsoft.Diagnostics.Tracing.TraceEvent/)

1. (Not important:) [TraceEvent Nuget package has moved from Prelease to Stable](https://blogs.msdn.microsoft.com/vancem/2014/01/03/traceevent-nuget-package-has-moved-from-prelease-to-stable/)

1. [Walk Through Getting Started with ETW TraceEvent NuGet Samples package](https://blogs.msdn.microsoft.com/vancem/2014/03/15/walk-through-getting-started-with-etw-traceevent-nuget-samples-package/)

1. (Not important:) [Version 1.1.24 of the EventSource NuGet Package marked as STABLE](https://blogs.msdn.microsoft.com/vancem/2015/05/11/version-1-1-24-of-the-eventsource-nuget-package-marked-as-stable/)

1. [Exploring EventSource Activity (correlation and causation) Features](https://blogs.msdn.microsoft.com/vancem/2015/09/14/exploring-eventsource-activity-correlation-and-causation-features/)
: item linked in the post: [link](resources/official/EventSourceActivities.docx)

1. [EventSource Activity Support Demo Code](https://blogs.msdn.microsoft.com/vancem/2015/09/15/eventsource-activity-support-demo-code/)
: item linked in the post: [link](resources/official/EventSourceActivityDemo.zip)

1. [Rich Payload Data in EventSource V4.6.](https://blogs.msdn.microsoft.com/vancem/2015/09/20/rich-payload-data-in-eventsource-v4-6/)
: item linked in the post: [link](resources/official/EventSourceRichPayloads.docx)

1. [Dynamically Defined Events in EventSource V4.6](https://blogs.msdn.microsoft.com/vancem/2015/10/02/dynamically-defined-events-in-eventsource-v4-6/)
: item linked in the post: [link](resources/official/EventSourceDynamicEvents.docx)

## General ETW-related topics
[Event Tracing portal (windows desktop, low level C/C++)](https://docs.microsoft.com/en-us/windows/desktop/etw/event-tracing-portal)
